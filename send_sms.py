# initial test file used to test only the text message functionality. Later scaled in distance.py file to
# incorporate the rest of the project
# Source code: https://www.twilio.com/docs/libraries/python 


import os
from twilio.rest import Client

def send_text_message(destination: str, message: str):

    account_sid = os.environ['TWILIO_ACCOUNT_SID']
    auth_token = os.environ['TWILIO_AUTH_TOKEN']
    client = Client(account_sid, auth_token)

    message = client.messages \
        .create(
            body=message,
            from_='+19705361800',
            to=destination
        )

    print(message.sid)

def main():
    send_text_message('+14089135710', "Your laundry hamper is full!")

if __name__ == "__main__":
    main()
