import RPi.GPIO as GPIO
import time
from twilio.rest import Client
import os


# Function used to send the text message 
# Source code: https://www.twilio.com/docs/libraries/python

def send_text_message(destination: str, message: str):

    account_sid = os.environ['TWILIO_ACCOUNT_SID'] # Stored in from the os
    auth_token = os.environ['TWILIO_AUTH_TOKEN']
    client = Client(account_sid, auth_token)

    message = client.messages \
        .create(
            body=message,
            from_='+19705361800',
            to=destination
        )

    print(message.sid)


# Main Function 
# Source code: https://thepihut.com/blogs/raspberry-pi-tutorials/hc-sr04-ultrasonic-range-sensor-on-the-raspberry-pi

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
TRIG = 23
ECHO = 24
temp = []
phone_number = os.environ['PHONE_NUMBER']
print(phone_number)

while True:
    GPIO.setup(ECHO,GPIO.IN)
    GPIO.setup(TRIG,GPIO.OUT)
    GPIO.output(TRIG, False)
    time.sleep(1)
    GPIO.output(TRIG, True)
    time.sleep(0.00001)
    GPIO.output(TRIG, False)

    while GPIO.input(ECHO)==0:
        pulse_start = time.time()

    while GPIO.input(ECHO)==1:
        pulse_end = time.time()

    pulse_duration = pulse_end - pulse_start

    distance = pulse_duration * 17150
    distance = round(distance, 2)

    print("Distance:",distance,"cm")

    temp_dist = int(distance)
    temp.append(temp_dist)



    if(len(temp) == 5):
        if(temp[0] < 20  and temp[1] < 20 and temp[2] < 20 and temp[3] < 20 and temp[4] < 20):
            print("Sending text")
            send_text_message(phone_number, "Your laundry hamper is full!") # Making our API call 
            break
        else:
            temp = []



    

GPIO.cleanup()
